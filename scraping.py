# Webscraping to get calendar and latest news and save it to json file

#!/usr/bin/python3
# -*- coding: utf-8 -*-

# sudo pip3 install opencv-python qrcode numpy requests bs4 selenium


import requests
import bs4
import re
import time
import urllib.request
import qrcode
import json
import textwrap


def main():
    #variables
    newsURL= "https://www.voka.be/nieuws?customized=true&region%5B%5D=134"
    calendarURL= "https://www.voka.be/activiteiten?customized=true&region=134"

    getNews(newsURL)
    getCalendar(calendarURL)


def downloadImage(imageURL,filename):
    image = urllib.request.urlopen(imageURL)
    with open("./img/" + filename, "wb") as f:
        f.write(image.read())

        
def getQRCode(newsLink,filename):
    vokaURL = "https://www.voka.be"
    file = "./img/" + filename
    # generate qr code
    newsLink = vokaURL + newsLink
    img = qrcode.make(newsLink)
    # save img to a file
    img.save(file)


def getNews(newsURL):
    newsdata = requests.get(newsURL)
    cleannewsdata = bs4.BeautifulSoup(newsdata.text, 'html.parser')

    newstitle= cleannewsdata.findAll('h3', {"class" : "card__title"})
    newssummary = cleannewsdata.findAll('div', {"class" : "card__description"})
    newsdate = cleannewsdata.findAll('time', {"class" : "card__date"})
    newsimage = cleannewsdata.findAll('img', {"class" : "card__img"})
    newslink = cleannewsdata.findAll('a', {"class" : "js-clickthrough__target"})

    writeNewstoJson(newstitle,newssummary,newsdate,newsimage,newslink)


def getCalendar(calendarURL):
    calendardata = requests.get(calendarURL)
    cleancalendardata = bs4.BeautifulSoup(calendardata.text, 'html.parser')

    eventcategory = cleancalendardata.findAll('span', {"class" : "card__category"})
    eventdate = cleancalendardata.findAll('time', {"class" : "card__date"})
    eventtitle= cleancalendardata.findAll('h2', {"class" : "card__title"})

    writeCalendartoJson(eventdate,eventtitle,eventcategory)




def writeNewstoJson(newstitle,newssummary,newsdate,newsimage,newslink):
    try:
        with open('./scraping.json', 'r') as f:
            scrapingdata = json.load(f)
    except ValueError:
        print ('Error: Decoding scrapingdata has failed')

    #newsitem0
    downloadImage(newsimage[0].attrs['src'],"news0.jpg")
    getQRCode(newslink[0].attrs['href'],"QR0.jpg")
    news_summary = newssummary[0].text
    news_summary = textwrap.shorten(news_summary, width=189)
    news_title0 = newstitle[0].text
    news_title0 = news_title0.strip()
    scrapingdata['NEWS0']['title'] = news_title0
    scrapingdata['NEWS0']['summary'] = news_summary
    scrapingdata['NEWS0']['date'] = newsdate[0].text
    

    #newsitem1
    downloadImage(newsimage[1].attrs['src'],"news1.jpg")
    getQRCode(newslink[1].attrs['href'],"QR1.jpg")
    news_summary = newssummary[1].text
    news_summary = textwrap.shorten(news_summary, width=189)
    news_title1 = newstitle[1].text
    news_title1 = news_title1.strip()
    scrapingdata['NEWS1']['title'] = news_title1
    scrapingdata['NEWS1']['summary'] = news_summary
    scrapingdata['NEWS1']['date'] = newsdate[1].text
    

    with open('./scraping.json', 'w') as f:
        json.dump(scrapingdata, f)



def writeCalendartoJson(eventdate,eventtitle,eventcategory):
    try:
        with open('./scraping.json', 'r') as f:
            scrapingdata = json.load(f)
    except ValueError:
        print ('Error: Decoding scrapingdata has failed')
    
    #get first 6 results
    scrapingdata['EVENT0']['date'] = eventdate[0].get_text()
    event_title0 = eventtitle[0].text
    scrapingdata['EVENT0']['title'] = event_title0.strip()
    scrapingdata['EVENT0']['category'] = eventcategory[0].get_text()
    scrapingdata['EVENT1']['date'] = eventdate[1].get_text()
    event_title1 = eventtitle[1].text
    scrapingdata['EVENT1']['title'] = event_title1.strip()
    scrapingdata['EVENT1']['category'] = eventcategory[1].get_text()
    scrapingdata['EVENT2']['date'] = eventdate[2].get_text()
    event_title2 = eventtitle[2].text
    scrapingdata['EVENT2']['title'] = event_title2.strip()
    scrapingdata['EVENT2']['category'] = eventcategory[2].get_text()
    scrapingdata['EVENT3']['date'] = eventdate[3].get_text()
    event_title3 = eventtitle[3].text
    scrapingdata['EVENT3']['title'] = event_title3.strip()
    scrapingdata['EVENT3']['category'] = eventcategory[3].get_text()
    scrapingdata['EVENT4']['date'] = eventdate[4].get_text()
    event_title4 = eventtitle[4].text
    scrapingdata['EVENT4']['title'] = event_title4.strip()
    scrapingdata['EVENT4']['category'] = eventcategory[4].get_text()
    scrapingdata['EVENT5']['date'] = eventdate[5].get_text()
    event_title5 = eventtitle[5].text
    scrapingdata['EVENT5']['title'] = event_title5.strip()
    scrapingdata['EVENT5']['category'] = eventcategory[5].get_text()
    

    with open('./scraping.json', 'w') as f:
        json.dump(scrapingdata, f)



if __name__ == "__main__":
    main()
