#!/usr/bin/python
# -*- coding: utf-8 -*-
import pygame
import time
import sys
import os
import random
import json
from datetime import datetime
from subprocess import Popen
from pygame.locals import *
from tools.telegram import send_telegram


# changing colors/images
def load_GRAY():
    COLORSGRAY = [(66, 66, 66),(33, 33, 2),(0, 0, 0)]
    return random.choice(COLORSGRAY)

def load_ORANGE():
    COLORSORANGE = [(255, 106, 57),(255, 175, 174),(255, 220, 120)]
    return random.choice(COLORSORANGE)

def load_RECHTS():
    IMAGERECHTS = [('R1.jpg'),('R2.jpg'),('R3.jpg')]
    return random.choice(IMAGERECHTS)

# fixed colors
COLORGREEN = (9,181,137)
WHITE = (255, 255, 255)
BLACK = (0,0,0)


def get_CONFIG():
    try:
        #with open('/home/kevinvdb/Code/welkomscherm_conf.json', 'r') as f:
        with open('/home/pi/Desktop/welkomscherm_conf.json', 'r') as f:
            CONFIG = json.load(f)
            return CONFIG
    except ValueError:
        print ('Error: Decoding CONFIG has failed')
        exit(1)

pygame.init()


CONFIG = get_CONFIG()
LOCATION = CONFIG['LOCATION']['vestiging']
LOCATION_DETAIL = CONFIG['LOCATION']['locatie']

# wrap news text function
def blit_text(surface, text, pos, font, color):
    words = [word.split(' ') for word in text.splitlines()]  # 2D array where each row is a list of words.
    space = font.size(' ')[0]  # The width of a space.
    max_width, max_height = surface.get_size()
    x, y = pos
    for line in words:
        for word in line:
            word_surface = font.render(word, 0, color)
            word_width, word_height = word_surface.get_size()
            if x + word_width >= max_width:
                x = pos[0]  # Reset the x.
                y += word_height  # Start on new row.
            surface.blit(word_surface, (x, y))
            x += word_width + space
        x = pos[0]  # Reset the x.
        y += word_height  # Start on new row.

# starting message to Telegram
message= LOCATION +" - "+LOCATION_DETAIL + " welkomscherm is opgestart"
#send_telegram(message)


# media files
LOCALPATHFILE = '/home/pi/welkomscherm/'
#LOCALPATHFILE = '/home/kevinvdb/Code/welkomscherm/'
LOCALEPATHIMAGE = LOCALPATHFILE + 'img/'
LOCALEPATHFONT =  LOCALPATHFILE + 'font/'
MOVIE = '/home/pi/Desktop/jaaroverzicht.mp4'
REPEATSONG = LOCALPATHFILE + 'media/repeat.mp3'
STARTTUNE = LOCALPATHFILE + 'media/zelda_level_up.mp3'
WAITTIME = 10  # default time to wait between images (in seconds)

EVENT_DATE = datetime.strptime("2024-06-14", "%Y-%m-%d") # edit this to the date of event year-month-day
EVENT_DATEW1 = datetime.strptime("2021-10-19", "%Y-%m-%d") # edit this to the date of event year-month-day
EVENT_DATEW2 = datetime.strptime("2021-10-20", "%Y-%m-%d") # edit this to the date of event year-month-day

# set up the window, max screensize, fullscreen no frames

modes = pygame.display.list_modes()
screen = pygame.display.set_mode(max(modes), pygame.NOFRAME)
pygame.mouse.set_visible(False)
(w, h) = screen.get_size() #x=width,y=height

# locations and resizing
# TV Waasland resolutie 1920x1080 as refence to other resolutions
S_WIDTH = w
S_HEIGHT = h
TOP = 0
L_STARTUP_W = int(700 * w / 1920)
L_STARTUP_H = int(540 * h / 1080)
L_WELCOME_W = int(100 * w / 1920)
L_WELCOME_H = int(800 * h / 1080)
L_LOGO_W = int(100 * w / 1920)
L_LOGO_H = int(250 * h / 1080)
L_EVENT_W = int(100 * w / 1920)
L_EVENTN_H = int(300 * h / 1080)
L_EVENTT_H = int(500 * h / 1080)
L_EVENTL_H = int(700 * h / 1080)
L_KA_TITEL_W = int(100 * w / 1920)
L_KA_TITEL_H = int(100 * h / 1080)
L_KA_ED_W = int(100 * w / 1920)  # horizontaal afstand voor kameragenda datum
L_KA_ET_W = int(100 * w / 1920)  # horizontaal afstand voor kameragenda event
L_KA_3_E1_H = int(300 * h / 1080)
L_KA_3_E2_H = int(500 * h / 1080)
L_KA_3_E3_H = int(700 * h / 1080)
L_KA_2_E1_H = int(500 * h / 1080)
L_KA_2_E2_H = int(800 * h / 1080)
L_KA_1_E1_H = int(500 * h / 1080)
L_KA_3_T1_H = int(375 * h / 1080)
L_KA_3_T2_H = int(575 * h / 1080)
L_KA_3_T3_H = int(775 * h / 1080)
L_KA_2_T1_H = int(575 * h / 1080)
L_KA_2_T2_H = int(875 * h / 1080)
L_KA_1_T1_H = int(575 * h / 1080)
L_RIGHT_W = int(1252 * w // 1920)       # positie rechtse afbeelding
L_RIGHT_H = 0                     # positie rechtse afbeelding
L_SCALE_RIGHT_W = int(668 * w // 1920)  # schalen van rechtse afbeelding
L_SCALE_RIGHT_H = h               # schalen van rechtse afbeelding
S_QR = int(150 * h / 1080)
L_QR_W = int(1508 * w / 1920)
L_QR_H = int(930 * h / 1080)
L_MARGIN_W = int(50 * w / 1920)
L_READMORE_W = int(900 * w / 1920)
L_READMORE_H = int(990 * h / 1080)
S_NEWS_W = int(698 * w / 1920)
S_NEWS_H = int(465 * h / 1080)
L_NEWS_W = int(960 * w / 1920)
L_ORANGEBAR_H = int(930 * h / 1080)
S_ORANGEBAR_H = int(200 * h / 1080)
L_NEWSLABEL_H = int(150 * h / 1080)
L_NEWSDATE_H = int(250 * h / 1080)
L_NEWSTITLE_H = int(580 * h / 1080)
L_NEWSSUMMARY_H = int(700 * h / 1080)
L_CALENDARLABEL_H = int(50 * h / 1080)
S_CALENDAR_H = int(150 * h / 1080)
L_EDATE0_W = int(100 * w / 1920)
L_EDATE0_H = int(250 * h / 1080)
L_ECATEGORY0_W = int(500 * w / 1920)
L_ECATEGORY0_H = int(250 * h / 1080)
L_ETITLE0_W = int(100 * w / 1920)
L_ETITLE0_H = int(300 * h / 1080)
L_EDATE1_W = int(100 * w / 1920)
L_EDATE1_H = int(500 * h / 1080)
L_ECATEGORY1_W = int(500 * w / 1920)
L_ECATEGORY1_H = int(500 * h / 1080)
L_ETITLE1_W = int(100 * w / 1920)
L_ETITLE1_H = int(550 * h / 1080)
L_EDATE2_W = int(100 * w / 1920)
L_EDATE2_H = int(750 * h / 1080)
L_ECATEGORY2_W = int(500 * w / 1920)
L_ECATEGORY2_H = int(750 * h / 1080)
L_ETITLE2_W = int(100 * w / 1920)
L_ETITLE2_H = int(800 * h / 1080)
L_PAGENUMBER_W= int(1700 * w / 1920)
L_COUNTDOWN_W = 0
L_COUNTDOWN_H = int(1070 * h / 1080)
S_COUNTDOWN_H = int(10 * h / 1080)
F_STARTUP = int(25 * h / 1080)
F_TEXT = int(35 * h / 1080)
F_TITLE = int(40 * h / 1080)
F_TITLE1 = int(75 * h / 1080)
F_TITLE2 = int(50 * h / 1080)
F_THICKNESS = int(40 * h / 1080)


# fonts
STARTUP = pygame.font.Font(LOCALEPATHFONT + 'Rubik-Regular.ttf', F_STARTUP)
TEXT = pygame.font.Font(LOCALEPATHFONT + 'Rubik-Regular.ttf', F_TEXT, bold=True)
TITLE = pygame.font.Font(LOCALEPATHFONT + 'Rubik-Regular.ttf', F_TITLE, bold=True)
TITLE2 = pygame.font.Font(LOCALEPATHFONT + 'Rubik-Bold.ttf', F_TITLE2, bold=True)
TITLE1 = pygame.font.Font(LOCALEPATHFONT + 'Rubik-Bold.ttf', F_TITLE1, bold=True)
THICKNESS = F_THICKNESS


# startup screen

label = STARTUP.render('... warming up, please wait ...', 5, WHITE)
screen.blit(label, (L_STARTUP_W, L_STARTUP_H))
pygame.display.flip()
#pygame.mixer.music.load(STARTTUNE)
#pygame.mixer.music.play(0)

# image CONFIG

logo = pygame.image.load(LOCALEPATHIMAGE + 'VOKA_AW_LOGO_CMYK668.jpg')
#sms = pygame.image.load(LOCALEPATHIMAGE + 'ESF.png')
#sms = sms.convert()
#sms = pygame.transform.scale(sms, max(modes))

nieuws = pygame.image.load(LOCALEPATHIMAGE + 'nieuws.jpg')
nieuws = nieuws.convert()
nieuws = pygame.transform.scale(nieuws, max(modes))

event = pygame.image.load(LOCALEPATHIMAGE +'scherm.jpg')
event = event.convert()
event = pygame.transform.scale(event, max(modes))

waas0 = pygame.image.load(LOCALEPATHIMAGE +'waasland0.png')
waas0 = waas0.convert()
waas0 = pygame.transform.scale(waas0, max(modes))

waas1 = pygame.image.load(LOCALEPATHIMAGE +'waasland1.png')
waas1 = waas1.convert()
waas1 = pygame.transform.scale(waas1, max(modes))

waas2 = pygame.image.load(LOCALEPATHIMAGE +'waasland2.png')
waas2 = waas2.convert()
waas2 = pygame.transform.scale(waas2, max(modes))

waas3 = pygame.image.load(LOCALEPATHIMAGE +'waasland3.png')
waas3 = waas3.convert()
waas3 = pygame.transform.scale(waas3, max(modes))

waas4 = pygame.image.load(LOCALEPATHIMAGE +'waasland4.png')
waas4 = waas4.convert()
waas4 = pygame.transform.scale(waas4, max(modes))

partner1 = pygame.image.load(LOCALEPATHIMAGE + 'PP_CORP.png')
partner1 = partner1.convert()
partner1 = pygame.transform.scale(partner1, max(modes))
#partner2 = pygame.image.load(LOCALEPATHIMAGE + 'PP_BUSIACT.jpg')
#partner2 = partner2.convert()
#partner2 = pygame.transform.scale(partner2, max(modes))

def countdownbar():
    remainingtime = WAITTIME
    while remainingtime > 0:
    # calculate size of timebar 
        S_TIMERBAR_W = S_WIDTH * remainingtime / WAITTIME
    # draw background of timerbar
        pygame.draw.rect(screen,BLACK,(L_COUNTDOWN_W, L_COUNTDOWN_H, S_WIDTH, S_COUNTDOWN_H))
    # draw timerbar 
        pygame.draw.rect(screen,COLORGREEN,(L_COUNTDOWN_W,L_COUNTDOWN_H,S_TIMERBAR_W,S_COUNTDOWN_H))
        pygame.display.flip()
        remainingtime = remainingtime - 0.125
        pygame.time.wait(125)

# run the loop

while True:
## ----------------------------------------------------------------------------- screen ---------------------------------------------------------------- ##
    # startscherm
    
    # play background sound zenfm radio in Auditorium
    if LOCATION_DETAIL == 'Auditorium':
        try:
            pygame.mixer.init()
            pygame.mixer.music.load(REPEATSONG)
            pygame.mixer.music.play()
        except:
            pass
    # tekenen van welkom scherm ipv afbeelding
    if LOCATION == 'Antwerpen':
        screen.fill(WHITE)
        ORANGE = load_ORANGE()
        GRAY = load_GRAY()
        RECHTS = pygame.image.load(LOCALEPATHIMAGE + load_RECHTS())
        RECHTS = RECHTS.convert()
        RECHTS = pygame.transform.scale(RECHTS, (L_SCALE_RIGHT_W,L_SCALE_RIGHT_H))
    

    # render text
        welcome = TITLE1.render('VAN HARTE WELKOM', 5, GRAY)
        screen.blit(welcome, (L_WELCOME_W, L_WELCOME_H))

        screen.blit(RECHTS,(L_RIGHT_W, L_RIGHT_H))
    
    # add logo
        screen.blit(logo, pygame.rect.Rect(L_LOGO_W, L_LOGO_H, 128, 128))
    
    # show above items in screen
        pygame.display.flip()

    # countdownbar
        countdownbar()
        



## ----------------------------------------------------------------------------- screen ---------------------------------------------------------------- ##
    # old news block
## ----------------------------------------------------------------------------- screen ---------------------------------------------------------------- ##
#    # reclame sms
#    if LOCATION == 'Antwerpen':
#        screen.blit(sms, (0, 0))
#        pygame.display.flip()
#
#        # countdownbar
#        countdownbar()


## ----------------------------------------------------------------------------- screen ---------------------------------------------------------------- ##
    #reclame hoofdevent
    if EVENT_DATE >= datetime.now():
        screen.blit(event,(0,0))
        # show above items in screen
        pygame.display.flip()

        # countdownbar
        countdownbar()


## ----------------------------------------------------------------------------- screen ---------------------------------------------------------------- ##

## ----------------------------------------------------------------------------- screen ---------------------------------------------------------------- ##
    #welcome waasland
    if LOCATION == 'Waasland':
        screen.blit(waas0,(0,0))
        # show above items in screen
        pygame.display.flip()

        # countdownbar
        countdownbar()



## ----------------------------------------------------------------------------- screen ---------------------------------------------------------------- ##
    if LOCATION == 'Antwerpen' or LOCATION == 'Waasland':
    # calendar
        screen.fill(WHITE)
        ORANGE = load_ORANGE()
        GRAY = load_GRAY()

    # load scrape data
        try:
            with open(LOCALPATHFILE + 'scraping.json', 'r') as f:
                scrapingdata = json.load(f)
                eventdate0 = scrapingdata['EVENT0']['date']
                eventtitle0 = scrapingdata['EVENT0']['title'] 
                eventcategory0 = scrapingdata['EVENT0']['category']
                eventdate1 = scrapingdata['EVENT1']['date']
                eventtitle1 = scrapingdata['EVENT1']['title'] 
                eventcategory1 = scrapingdata['EVENT1']['category'] 
                eventdate2 = scrapingdata['EVENT2']['date']
                eventtitle2 = scrapingdata['EVENT2']['title']
                eventcategory2 = scrapingdata['EVENT2']['category'] 
                eventdate3 = scrapingdata['EVENT3']['date']
                eventtitle3 = scrapingdata['EVENT3']['title'] 
                eventcategory3 = scrapingdata['EVENT3']['category'] 
                eventdate4 = scrapingdata['EVENT4']['date'] 
                eventtitle4 = scrapingdata['EVENT4']['title'] 
                eventcategory4 = scrapingdata['EVENT4']['category'] 
                eventdate5 = scrapingdata['EVENT5']['date'] 
                eventtitle5 = scrapingdata['EVENT5']['title'] 
                eventcategory5 = scrapingdata['EVENT5']['category'] 

            ## screen 1/2

            # orange bar above
            #pygame.draw.rect(screen, [red, blue, green], [left, top, width, height], filled)
                pygame.draw.rect(screen,ORANGE,(TOP,TOP,S_WIDTH,S_CALENDAR_H))

            # orange bar below
            #pygame.draw.rect(screen, [red, blue, green], [left, top, width, height], filled)
                pygame.draw.rect(screen,ORANGE,(TOP,L_ORANGEBAR_H,S_WIDTH,S_ORANGEBAR_H))

            # calendar label
                calendar_label = TITLE1.render('TOEKOMSTIGE EVENTS', 5, GRAY)
                screen.blit(calendar_label, (L_WELCOME_W, L_CALENDARLABEL_H))

            # pagenumber label
                pagenumber_label = TITLE.render('(1/2)', 5, GRAY)
                screen.blit(pagenumber_label, (L_PAGENUMBER_W, L_CALENDARLABEL_H))

            # date event 0 
                edate0 = TEXT.render(eventdate0, 2, GRAY)
                screen.blit(edate0, (L_EDATE0_W, L_EDATE0_H))
            # category event 0 
                ecategory0 = TEXT.render(eventcategory0, 2, ORANGE)
                screen.blit(ecategory0, (L_ECATEGORY0_W, L_ECATEGORY0_H))
            # title event 0 
                blit_text(screen, eventtitle0, (L_ETITLE0_W, L_ETITLE0_H), TITLE2, GRAY)
            
            # date event 1 
                edate1 = TEXT.render(eventdate1, 2, GRAY)
                screen.blit(edate1, (L_EDATE1_W, L_EDATE1_H))
            # category event 1 
                ecategory1 = TEXT.render(eventcategory1, 2, ORANGE)
                screen.blit(ecategory1, (L_ECATEGORY1_W, L_ECATEGORY1_H))
            # title event 1
                blit_text(screen, eventtitle1, (L_ETITLE1_W, L_ETITLE1_H), TITLE2, GRAY)

            # date event 2 
                edate2 = TEXT.render(eventdate2, 2, GRAY)
                screen.blit(edate2, (L_EDATE2_W, L_EDATE2_H))
            # category event 2
                ecategory2 = TEXT.render(eventcategory2, 2, ORANGE)
                screen.blit(ecategory2, (L_ECATEGORY2_W, L_ECATEGORY2_H))
            # title event 2
                blit_text(screen, eventtitle2, (L_ETITLE2_W, L_ETITLE2_H), TITLE2, GRAY)

            # scan QR code text
                read_more = TEXT.render('Scan de QR code en schrijf je in', 2, GRAY)
                screen.blit(read_more, (L_READMORE_W, L_READMORE_H))

            # add QR code
                QR = pygame.image.load(LOCALEPATHIMAGE + 'QR_calendar.jpg')
                QR = pygame.transform.scale(QR, (S_QR, S_QR))
                screen.blit(QR, pygame.rect.Rect(L_QR_W, L_QR_H, 50, 50))

            # show above items in screen
                pygame.display.flip()

            # countdownbar
                countdownbar()

            ## screen 2/2
                screen.fill(WHITE)

            # orange bar above
            #pygame.draw.rect(screen, [red, blue, green], [left, top, width, height], filled)
                pygame.draw.rect(screen,ORANGE,(TOP,TOP,S_WIDTH,S_CALENDAR_H))

            # orange bar below
            #pygame.draw.rect(screen, [red, blue, green], [left, top, width, height], filled)
                pygame.draw.rect(screen,ORANGE,(TOP,L_ORANGEBAR_H,S_WIDTH,S_ORANGEBAR_H))

            # calendar label
                calendar_label = TITLE1.render('TOEKOMSTIGE EVENTS', 5, GRAY)
                screen.blit(calendar_label, (L_WELCOME_W, L_CALENDARLABEL_H))

            # pagenumber label
                pagenumber_label = TITLE.render('(2/2)', 5, GRAY)
                screen.blit(pagenumber_label, (L_PAGENUMBER_W, L_CALENDARLABEL_H))

            # date event 3 
                edate3 = TEXT.render(eventdate3, 2, GRAY)
                screen.blit(edate3, (L_EDATE0_W, L_EDATE0_H))
            # category event 3 
                ecategory3 = TEXT.render(eventcategory3, 2, ORANGE)
                screen.blit(ecategory3, (L_ECATEGORY0_W, L_ECATEGORY0_H))
            # title event 3
                blit_text(screen, eventtitle3, (L_ETITLE0_W, L_ETITLE0_H), TITLE2, GRAY)
            
            # date event 4
                edate4 = TEXT.render(eventdate4, 2, GRAY)
                screen.blit(edate4, (L_EDATE1_W, L_EDATE1_H))
            # category event 4 
                ecategory4 = TEXT.render(eventcategory4, 2, ORANGE)
                screen.blit(ecategory4, (L_ECATEGORY1_W, L_ECATEGORY1_H))
            # title event 4
                blit_text(screen, eventtitle4, (L_ETITLE1_W, L_ETITLE1_H), TITLE2, GRAY)

            # date event 5 
                edate5 = TEXT.render(eventdate5, 2, GRAY)
                screen.blit(edate5, (L_EDATE2_W, L_EDATE2_H))
            # category event 5
                ecategory5 = TEXT.render(eventcategory5, 2, ORANGE)
                screen.blit(ecategory5, (L_ECATEGORY2_W, L_ECATEGORY2_H))
            # title event 5
                blit_text(screen, eventtitle5, (L_ETITLE2_W, L_ETITLE2_H), TITLE2, GRAY)

            # scan QR code text
                read_more = TEXT.render('Scan de QR code en schrijf je in', 2, GRAY)
                screen.blit(read_more, (L_READMORE_W, L_READMORE_H))

            # add QR code
                QR = pygame.image.load(LOCALEPATHIMAGE + 'QR_calendar.jpg')
                QR = pygame.transform.scale(QR, (S_QR, S_QR))
                screen.blit(QR, pygame.rect.Rect(L_QR_W, L_QR_H, 50, 50))

            # show above items in screen
                pygame.display.flip()

            # countdownbar
                countdownbar()
        except ValueError:
            print ('Error: Decoding scrapingdata has failed',ValueError)
            pass



## ----------------------------------------------------------------------------- screen ---------------------------------------------------------------- ##
    # partners Waasland

    if LOCATION == 'Waasland':
        screen.blit(partner1, (0, 0))
        # show above items in screen
        pygame.display.flip()

        # countdownbar
        countdownbar()
        

## ----------------------------------------------------------------------------- screen ---------------------------------------------------------------- ##
    # jaarverslag film Antwerpen Wintertuin

    minute = time.strftime("%M")
    movie_check = int(minute) % 10

    if movie_check == 0 and LOCATION_DETAIL == 'Wintertuin':
        try:
            minute_played = open('./movie.tmp','r').read()
        except IOError:
            print ("Error: movie.tmp does not appear to exist.")
            minute_played = "OK"
      

        if minute_played != minute or minute_played == "OK":
            omxp = Popen(['omxplayer', MOVIE])
            try:
                with open('./movie.tmp', 'w') as movielog:
                    movielog.write(minute)
            except IOError:
                print ("Error: movie.tmp cannot be made.")

       

